#####################
import sys,re,os
import argparse
import textwrap
import pysam as ps
import numpy as np
import pandas as pd
from collections import Counter
from multiprocessing import Pool
#####################
######################################################################################################### functions
def SA_Reader(bed,bam,outdir,
				I_mq,I_nm,I_cl,F_cr
				) :
	#----------------------------------------------------------------------------------------------------
	df_inBED = pd.read_csv(bed,sep='\t',names=['chr','start','end','gene'])
	Ps_inBAM = ps.AlignmentFile(bam,'rb')
	S_inName = bam.split('/')[-1].split('.')[0]
	#----------------------------------------------------------------------------------------------------
	L_Rows = []
	for I_index in df_inBED.index :
		S_BEDchr = df_inBED['chr'][I_index]
		I_BEDstart = df_inBED['start'][I_index]
		I_BEDend = df_inBED['end'][I_index]
		S_BEDgene = df_inBED['gene'][I_index]
		#------------------------------------------------------------------------------------------------
		for read in Ps_inBAM.fetch(S_BEDchr,I_BEDstart,I_BEDend) :
			S_Name = read.query_name
			S_Chr = read.reference_name
			S_Cigar = read.cigarstring
			I_RefStart = read.reference_start+1 # pysam is ZERO base.
			I_TemplateLen = abs(read.template_length)
			I_MQ = read.mapping_quality
			I_NM = read.get_tag('NM')
			#--------------------------------------------------------------------------------------------
			I_ReadLen = read.reference_length
			I_QueryLength = I_ReadLen-1
			I_RefEnd = I_RefStart+I_QueryLength
			#--------------------------------------------------------------------------------------------
			S_DupCheck = read.is_duplicate
			#--------------------------------------------------------------------------------------------
			try :
				S_SA = read.get_tag('SA')
				L_SAs = S_SA.split(';')[:-1]
			except :
				S_SA = '-'
			if read.is_reverse :
				S_Direction = '-'
			else :
				S_Direction = '+'
			#-------------------------------------------------------------------------------------------- Check duplicate & clip
			if S_DupCheck == False and S_SA != '-' :
				I_C_Len = sum(map(int, re.findall( '(\d+)S',S_Cigar )))+sum(map(int, re.findall( '(\d+)H',S_Cigar )))
				#---------------------------------------------------------------------------------------- Check break point
				L_Ori_Types = re.findall('\D',S_Cigar)
				S_Ori_StartType = L_Ori_Types[0]
				S_Ori_EndType = L_Ori_Types[-1]
				if (S_Ori_StartType == 'S' or S_Ori_StartType == 'H') and (S_Ori_EndType == 'S' or S_Ori_EndType == 'H') :
					S_Ori_BreakPoint = 'Both'
				elif S_Ori_StartType == 'S' or S_Ori_StartType == 'H' :
					S_Ori_BreakPoint = 'Front'
				elif S_Ori_EndType == 'S' or S_Ori_EndType == 'H' :
					S_Ori_BreakPoint = 'Back'
				L_Ori_inInfo = [S_Chr,I_RefStart,S_Direction,S_Cigar,I_MQ,I_NM,I_ReadLen,I_C_Len,I_RefEnd,S_Ori_BreakPoint,I_TemplateLen]
				L_OriRead = [S_Name,'Original']+L_Ori_inInfo
				L_Rows.append(L_OriRead)
				#----------------------------------------------------------------------------------------
				for L_SA in L_SAs :
					I_SA_Start = int(L_SA.split(',')[1])
					S_SA_Cigar = L_SA.split(',')[3]
					#------------------------------------------------------------------------------------ Check break point
					L_Sa_Types = re.findall('\D',S_SA_Cigar)
					S_Sa_StartType = L_Sa_Types[0]
					S_Sa_EndType = L_Sa_Types[-1]
					if (S_Sa_StartType == 'S' or S_Sa_StartType == 'H') and (S_Sa_EndType == 'S' or S_Sa_EndType == 'H') :
						S_Sa_BreakPoint = 'Both'
					elif S_Sa_StartType == 'S' or S_Sa_StartType == 'H' :
						S_Sa_BreakPoint = 'Front'
					elif S_Sa_EndType == 'S' or S_Sa_EndType == 'H' :
						S_Sa_BreakPoint = 'Back'
					#------------------------------------------------------------------------------------
					I_SA_M_Len = sum(map(int, re.findall( '(\d+)M',S_SA_Cigar )))
					I_SA_C_Len = sum(map(int, re.findall( '(\d+)S',S_SA_Cigar )))+sum(map(int, re.findall( '(\d+)H',S_SA_Cigar )))
					I_SA_End = I_SA_Start+I_SA_M_Len-1
					L_SARead = [S_Name,'SA']+L_SA.split(',')+[I_SA_M_Len,I_SA_C_Len,I_SA_End,S_Sa_BreakPoint,I_TemplateLen]
					L_Rows.append(L_SARead)
	#----------------------------------------------------------------------------------------------------
	df_RawInfo = pd.DataFrame(L_Rows, columns=['ID','type','chr','start','strand','cigar','MQ','NM','M_len','C_len','end','BreakPoint','FragLen'])
	df_RawInfo = df_RawInfo[['ID','type','chr','start','end','FragLen','BreakPoint','M_len','C_len','strand','cigar','MQ','NM']]
	A_SM_Ratio = round(abs(df_RawInfo['C_len'])/(df_RawInfo['C_len']+df_RawInfo['M_len'])*100,2)
	df_RawInfo.insert(loc=df_RawInfo.shape[1],column='ratio',value=A_SM_Ratio)
	df_RawInfo[['MQ','NM','C_len','start','end']] = df_RawInfo[['MQ','NM','C_len','start','end']].apply(pd.to_numeric)
	#----------------------------------------------------------------------------------------------------
	df_RawInfo.to_csv('{}/{}_ClipInfo.tsv'.format(outdir,S_inName),sep='\t',index=None)
	#---------------------------------------------------------------------------------------------------- Filter
	df_Selected = df_RawInfo[(df_RawInfo['MQ']<I_mq)|(I_nm<df_RawInfo['NM'])|(df_RawInfo['C_len']<I_cl)|(df_RawInfo['ratio']<F_cr)]
	df_Filtered = df_RawInfo.copy()
	for S_ID in set(df_Selected['ID']) :
		df_Filtered = df_Filtered[(df_Filtered['ID']!=S_ID)]
	#----------------------------------------------------------------------------------------------------
	return(df_Filtered)
#========================================================================================================
def FUSION_cluster(bed,bam,outdir,
				I_mq,I_nm,I_cl,F_cr
				) :
	#----------------------------------------------------------------------------------------------------
	df_Filtered = SA_Reader(bed,bam,outdir,I_mq,I_nm,I_cl,F_cr)
	S_inName = bam.split('/')[-1].split('.')[0]
	#----------------------------------------------------------------------------------------------------
	df_Filtered_Ori = df_Filtered[(df_Filtered['type']=='Original')]
	df_Filtered_Sa = df_Filtered[(df_Filtered['type']=='SA')]
	#---------------------------------------------------------------------------------------------------- Overlap check
	df_FilteredOri_Front = df_Filtered_Ori[(df_Filtered_Ori['BreakPoint']=='Front')].groupby(
											['ID','chr','start']).size().reset_index().groupby(
											['chr','start']).size().reset_index().rename(columns={'start':'pos',0:'OriFront'})
	df_FilteredOri_Back = df_Filtered_Ori[(df_Filtered_Ori['BreakPoint']=='Back')].groupby(
											['ID','chr','end']).size().reset_index().groupby(
											['chr','end']).size().reset_index().rename(columns={'end':'pos',0:'OriBack'})
	df_FilteredSa_Front = df_Filtered_Sa[(df_Filtered_Sa['BreakPoint']=='Front')].groupby(
											['ID','chr','start']).size().reset_index().groupby(
											['chr','start']).size().reset_index().rename(columns={'start':'pos',0:'SaFront'})
	df_FilteredSa_Back = df_Filtered_Sa[(df_Filtered_Sa['BreakPoint']=='Back')].groupby(
											['ID','chr','end']).size().reset_index().groupby(
											['chr','end']).size().reset_index().rename(columns={'end':'pos',0:'SaBack'})
	df_FilteredOri_BothStart = df_Filtered_Ori[(df_Filtered_Ori['BreakPoint']=='Both')].groupby(
											['ID','chr','start']).size().reset_index().groupby(
											['chr','start']).size().reset_index().rename(columns={'start':'pos',0:'OriBoth'})
	df_FilteredOri_BothEnd = df_Filtered_Ori[(df_Filtered_Ori['BreakPoint']=='Both')].groupby(
											['ID','chr','end']).size().reset_index().groupby(
											['chr','end']).size().reset_index().rename(columns={'end':'pos',0:'OriBoth'})
	df_FilteredSa_BothStart = df_Filtered_Sa[(df_Filtered_Sa['BreakPoint']=='Both')].groupby(
											['ID','chr','start']).size().reset_index().groupby(
											['chr','start']).size().reset_index().rename(columns={'start':'pos',0:'SaBoth'})
	df_FilteredSa_BothEnd = df_Filtered_Sa[(df_Filtered_Sa['BreakPoint']=='Both')].groupby(
											['ID','chr','end']).size().reset_index().groupby(
											['chr','end']).size().reset_index().rename(columns={'end':'pos',0:'SaBoth'})
	df_FilteredOri_Both = pd.concat([df_FilteredOri_BothStart,df_FilteredOri_BothEnd],ignore_index=True)
	df_FilteredSa_Both = pd.concat([df_FilteredSa_BothStart,df_FilteredSa_BothEnd],ignore_index=True)
	#---------------------------------------------------------------------------------------------------- Original & SA comparison
	df_OriSa_Front = pd.merge(df_FilteredOri_Front,df_FilteredSa_Front,how='outer',on=['chr','pos'])
	df_OriSa_Back = pd.merge(df_FilteredOri_Back,df_FilteredSa_Back,how='outer',on=['chr','pos'])
	df_OriSa_Both = pd.merge(df_FilteredOri_Both,df_FilteredSa_Both,how='outer',on=['chr','pos'])
	df_OriSa = pd.merge(df_OriSa_Front,df_OriSa_Back,how='outer',on=['chr','pos'])
	df_OriSa = pd.merge(df_OriSa,df_OriSa_Both,how='outer',on=['chr','pos']).replace(np.nan,0)
	A_OriSa_Sum = df_OriSa.iloc[:,2:].sum(axis=1)
	df_OriSa.insert(loc=df_OriSa.shape[1],column='depth',value=A_OriSa_Sum)
	df_OriSa.to_csv('{}/{}_ClusterInfo.tsv'.format(outdir,S_inName),sep='\t',index=None)
	return(df_OriSa)
#========================================================================================================
def FUSION_caller(bed,bam,outdir,
					I_mq,I_nm,I_cl,F_cr,
					I_al,I_sd
					) :
	#---------------------------------------------------------------------------------------------------- gene FUSION list
	D_TargetGenes = {"ALK": ["ADAM17", "AKAP8L", "ATAD2B", "ATP13A4", "BCL11A", "BIRC6",
							 "C12orf75", "CAMKMT", "CDK15", "CEBPZ", "CEP55", "CLIP1",
							 "CLIP4", "CLTC", "CMTR1", "CRIM1", "CUX1", "CYBRD1", "DCHS1",
							 "DCTN1", "DYSF", "EIF2AK3", "EML4", "EML6", "EPAS1", "ERC1",
							 "TOGARAM2", "FBN1", "FBXO11", "FBXO36", "FUT8", "GCC2",
							 "HIP1", "ITGAV", "KIF5B", "KLC1", "LCLAT1", "LIMD1", "LINC00211",
							 "LINC00327", "LMO7", "LOC349160", "LPIN1", "LYPD1", "MPRIP", "MTA3",
							 "MYT1L", "NCOA1", "NYAP2", "PHACTR1", "PICALM", "PLEKHA7", "PLEKHH2",
							 "PLEKHM2", "PPFIBP1", "PPM1B", "PRKAR1A", "PRKCB", "RBM20",
							 "SEC31A", "SLC16A7", "SLMAP", "SMPD4", "SOCS5", "SORCS1", "SOS1",
							 "SPECC1L", "SPTBN1", "SQSTM1", "SRBD1", "SRD5A2", "STRN", "SWAP70",
							 "TACR1", "TANC1", "TCF12", "TFG", "THADA", "TNIP2", "TPR", "TRIM66",
							 "TSPYL6", "TTC27", "VIT", "VKORC1L1", "WDPCP", "WDR37", "WNK3", "YAP1"
							 ]
					, "ROS1": ["CD74", "SLC34A2", "TPM3", "SDC4", "EZR", "LRIG3", "KDELR2", "CCDC6"] 
					, "RET": ["ADD3", "ANK3", "ANKS1B", "ARHGAP12", "CCDC186", "CCDC3", "CCDC6",
							  "CCDC88C", "CCNYL2", "CLIP1", "CTNNA3", "CUX1", "DOCK1", "DYDC1",
							  "EML4", "EML6", "EPC1", "EPHA5", "ERC1",
							  "GPR139", "GPRC5B", "KIAA1217", "RELCH", "KIF13A",
							  "KIF5B", "LSM14A", "MPRIP", "MYO5C", "NCOA4", "PARD3", "PCM1",
							  "PICALM", "PRKAR1A", "PRKCQ", "PRKG1", "PRPF18", "PTPRK", "RASSF4",
							  "RBPMS", "RUFY2", "SIRT1", "SORBS1", "TBC1D32", "TRIM24",
							  "TRIM33", "TSSK4", "WAC"
							  ]
					, "TMPRSS2": ["ERG"]}
	#----------------------------------------------------------------------------------------------------
	df_Filtered = SA_Reader(bed,bam,outdir,I_mq,I_nm,I_cl,F_cr)
	df_OriSa = FUSION_cluster(bed,bam,outdir,I_mq,I_nm,I_cl,F_cr)
	df_inBED = pd.read_csv(bed,sep='\t',names=['chr','start','end','gene'])
	Ps_inBAM = ps.AlignmentFile(bam,'rb')
	S_inName = bam.split('/')[-1].split('.')[0]
	#----------------------------------------------------------------------------------------------------
	L_RawDFs = []
	L_ReportDFs = []
	for S_GeneA, L_Genes in D_TargetGenes.items() :
		for S_GeneB in L_Genes :
			S_TargetFusion = '{}:{}'.format(S_GeneA,S_GeneB)
			S_GeneA_Chr = df_inBED[(df_inBED['gene']==S_GeneA)]['chr'].reset_index(drop=True)[0]
			S_GeneA_Start = df_inBED[(df_inBED['gene']==S_GeneA)]['start'].reset_index(drop=True)[0]
			S_GeneA_End = df_inBED[(df_inBED['gene']==S_GeneA)]['end'].reset_index(drop=True)[0]
			#--------------------------------------------------------------------------------------------
			S_GeneB_Chr = df_inBED[(df_inBED['gene']==S_GeneB)]['chr'].reset_index(drop=True)[0]
			S_GeneB_Start = df_inBED[(df_inBED['gene']==S_GeneB)]['start'].reset_index(drop=True)[0]
			S_GeneB_End = df_inBED[(df_inBED['gene']==S_GeneB)]['end'].reset_index(drop=True)[0]
			#--------------------------------------------------------------------------------------------
			df_GeneAInfo = df_OriSa[(df_OriSa['chr']==S_GeneA_Chr)&(S_GeneA_Start<=df_OriSa['pos'])&(df_OriSa['pos']<=S_GeneA_End)]
			df_GeneBInfo = df_OriSa[(df_OriSa['chr']==S_GeneB_Chr)&(S_GeneB_Start<=df_OriSa['pos'])&(df_OriSa['pos']<=S_GeneB_End)]
			df_GeneAInfo.insert(loc=2,column='gene',value=[S_GeneA]*df_GeneAInfo.shape[0])
			df_GeneBInfo.insert(loc=2,column='gene',value=[S_GeneB]*df_GeneBInfo.shape[0])
			#-------------------------------------------------------------------------------------------- GeneA
			df_GeneA_Candidates = df_GeneAInfo[(I_sd<df_GeneAInfo['OriFront'])|
												(I_sd<df_GeneAInfo['SaFront'])|
												(I_sd<df_GeneAInfo['OriBack'])|
												(I_sd<df_GeneAInfo['SaBack'])|
												(I_sd<df_GeneAInfo['OriBoth'])|
												(I_sd<df_GeneAInfo['SaBoth'])
												]
			#--------------------------------------------------------------------------------------------
			D_GeneA_FusionInfo = {}
			for I_GeneA_Main_pos in df_GeneA_Candidates['pos'] :
				I_GeneA_AllowStart = I_GeneA_Main_pos-I_al
				I_GeneA_AllowEnd = I_GeneA_Main_pos+I_al
				df_GeneA_Allowed = df_GeneAInfo[(I_GeneA_AllowStart<=df_GeneAInfo['pos'])&(df_GeneAInfo['pos']<=I_GeneA_AllowEnd)]
				D_GeneA_FusionInfo[I_GeneA_Main_pos] = {}
				#---------------------------------------------------------------------------------------- Break point check
				for I_index in df_GeneA_Allowed.index :
					I_GeneA_FrontCheck = df_GeneA_Allowed['OriFront'][I_index]+df_GeneA_Allowed['SaFront'][I_index]
					I_GeneA_BackCheck = df_GeneA_Allowed['OriBack'][I_index]+df_GeneA_Allowed['SaBack'][I_index]
					I_GeneA_BreakPoint = df_GeneA_Allowed['pos'][I_index]
					if I_GeneA_BackCheck < I_GeneA_FrontCheck :
						S_GeneA_BreakType = 'Front'
						df_GeneA_BreakCase = df_Filtered[(df_Filtered['BreakPoint']=='Front')|(df_Filtered['BreakPoint']=='Both')]
						L_GeneA_BreakIDs = list(set(df_GeneA_BreakCase[(df_GeneA_BreakCase['start']==I_GeneA_BreakPoint)]['ID']))
						D_GeneA_FusionInfo[I_GeneA_Main_pos][I_GeneA_BreakPoint] = [S_GeneA_BreakType,L_GeneA_BreakIDs]
					else :
						S_GeneA_BreakType = 'Back'
						df_GeneA_BreakCase = df_Filtered[(df_Filtered['BreakPoint']=='Back')|(df_Filtered['BreakPoint']=='Both')]
						L_GeneA_BreakIDs = list(set(df_GeneA_BreakCase[(df_GeneA_BreakCase['end']==I_GeneA_BreakPoint)]['ID']))
						D_GeneA_FusionInfo[I_GeneA_Main_pos][I_GeneA_BreakPoint] = [S_GeneA_BreakType,L_GeneA_BreakIDs]
			#-------------------------------------------------------------------------------------------- Multi-FUSION check
			L_GeneA_FusionInfo = []
			for I_MainPos, D_Infos in D_GeneA_FusionInfo.items() :
				S_MainPos_Type = D_Infos[I_MainPos][0]
				L_GeneA_Infos = []
				L_GeneA_IDs = []
				for I_Pos, L_Infos in D_Infos.items() :
					S_Pos_Type = L_Infos[0]
					if S_MainPos_Type == S_Pos_Type :
						L_Pos_IDs = L_Infos[-1]
						I_Pos_Depth = len(L_Pos_IDs)
						L_GeneA_Infos.append('{}:{}'.format(I_Pos,I_Pos_Depth))
						L_GeneA_IDs+=L_Pos_IDs
				S_GeneA_Infos = ','.join(L_GeneA_Infos)
				I_GeneA_Depth = len(set(L_GeneA_IDs))
				L_GeneA_FusionInfo.append( [S_GeneA,S_GeneA_Chr,I_MainPos,S_GeneA_Infos,S_MainPos_Type,I_GeneA_Depth,set(L_GeneA_IDs)] )
			#-------------------------------------------------------------------------------------------- GeneB
			df_GeneB_Candidates = df_GeneBInfo[(I_sd<df_GeneBInfo['OriFront'])|
												(I_sd<df_GeneBInfo['SaFront'])|
												(I_sd<df_GeneBInfo['OriBack'])|
												(I_sd<df_GeneBInfo['SaBack'])|
												(I_sd<df_GeneBInfo['OriBoth'])|
												(I_sd<df_GeneBInfo['SaBoth'])
												]
			#--------------------------------------------------------------------------------------------
			D_GeneB_FusionInfo = {}
			for I_GeneB_Main_pos in df_GeneB_Candidates['pos'] :
				I_GeneB_AllowStart = I_GeneB_Main_pos-I_al
				I_GeneB_AllowEnd = I_GeneB_Main_pos+I_al
				df_GeneB_Allowed = df_GeneBInfo[(I_GeneB_AllowStart<=df_GeneBInfo['pos'])&(df_GeneBInfo['pos']<=I_GeneB_AllowEnd)]
				D_GeneB_FusionInfo[I_GeneB_Main_pos] = {}
				#---------------------------------------------------------------------------------------- Break point check
				for I_index in df_GeneB_Allowed.index :
					I_GeneB_FrontCheck = df_GeneB_Allowed['OriFront'][I_index]+df_GeneB_Allowed['SaFront'][I_index]
					I_GeneB_BackCheck = df_GeneB_Allowed['OriBack'][I_index]+df_GeneB_Allowed['SaBack'][I_index]
					I_GeneB_BreakPoint = df_GeneB_Allowed['pos'][I_index]
					if I_GeneB_BackCheck < I_GeneB_FrontCheck :
						S_GeneB_BreakType = 'Front'
						df_GeneB_BreakCase = df_Filtered[(df_Filtered['BreakPoint']=='Front')|(df_Filtered['BreakPoint']=='Both')]
						L_GeneB_BreakIDs = list(set(df_GeneB_BreakCase[(df_GeneB_BreakCase['start']==I_GeneB_BreakPoint)]['ID']))
						D_GeneB_FusionInfo[I_GeneB_Main_pos][I_GeneB_BreakPoint] = [S_GeneB_BreakType,L_GeneB_BreakIDs]
					else :
						S_GeneB_BreakType = 'Back'
						df_GeneB_BreakCase = df_Filtered[(df_Filtered['BreakPoint']=='Back')|(df_Filtered['BreakPoint']=='Both')]
						L_GeneB_BreakIDs = list(set(df_GeneB_BreakCase[(df_GeneB_BreakCase['end']==I_GeneB_BreakPoint)]['ID']))
						D_GeneB_FusionInfo[I_GeneB_Main_pos][I_GeneB_BreakPoint] = [S_GeneB_BreakType,L_GeneB_BreakIDs]
			#-------------------------------------------------------------------------------------------- Multi-FUSION check
			L_GeneB_FusionInfo = []
			for I_MainPos, D_Infos in D_GeneB_FusionInfo.items() :
				S_MainPos_Type = D_Infos[I_MainPos][0]
				L_GeneB_Infos = []
				L_GeneB_IDs = []
				for I_Pos, L_Infos in D_Infos.items() :
					S_Pos_Type = L_Infos[0]
					if S_MainPos_Type == S_Pos_Type :
						L_Pos_IDs = L_Infos[-1]
						I_Pos_Depth = len(L_Pos_IDs)
						L_GeneB_Infos.append('{}:{}'.format(I_Pos,I_Pos_Depth))
						L_GeneB_IDs+=L_Pos_IDs
				S_GeneB_Infos = ','.join(L_GeneB_Infos)
				I_GeneB_Depth = len(set(L_GeneB_IDs))
				L_GeneB_FusionInfo.append( [S_GeneB,S_GeneB_Chr,I_MainPos,S_GeneB_Infos,S_MainPos_Type,I_GeneB_Depth,set(L_GeneB_IDs)] )
			#-------------------------------------------------------------------------------------------- GeneAB information
			df_GeneA_FusionInfo = pd.DataFrame(L_GeneA_FusionInfo,
												columns=['gene','chr','BreakPoint','info','type','depth','IDs']).groupby(
												['gene','chr','BreakPoint','info','type','depth'])['IDs'].apply(
												lambda x : set([ n for i in x for n in i ] )).reset_index()
			df_GeneB_FusionInfo = pd.DataFrame(L_GeneB_FusionInfo,
												columns=['gene','chr','BreakPoint','info','type','depth','IDs']).groupby(
												['gene','chr','BreakPoint','info','type','depth'])['IDs'].apply(
												lambda x : set([ n for i in x for n in i ] )).reset_index()
			#--------------------------------------------------------------------------------------------
			for I_GeneA_index in df_GeneA_FusionInfo.index :
				Set_GeneA_IDs = df_GeneA_FusionInfo['IDs'][I_GeneA_index]
				for I_GeneB_index in df_GeneB_FusionInfo.index :
					Set_GeneB_IDs = df_GeneB_FusionInfo['IDs'][I_GeneB_index]
					Set_InterSected = Set_GeneA_IDs.intersection(Set_GeneB_IDs)
					I_Alt = len(Set_InterSected)
					if I_sd < I_Alt :
						df_GeneA_Called = df_GeneA_FusionInfo.loc[[I_GeneA_index]].drop(columns=['IDs']).reset_index(drop=True)
						df_GeneB_Called = df_GeneB_FusionInfo.loc[[I_GeneB_index]].drop(columns=['IDs']).reset_index(drop=True)
						df_GeneAB_Called = pd.concat([df_GeneA_Called,df_GeneB_Called],ignore_index=True)
						I_DupCheck = df_GeneAB_Called.drop_duplicates().shape[0]
						if I_DupCheck == 2 :
							df_GeneAB_Called.insert(loc=0,column='Fusion',value=[S_TargetFusion]*df_GeneAB_Called.shape[0])
							L_RawDFs.append(df_GeneAB_Called)
							#----------------------------------------------------------------------------
							df_GeneA_Called = df_GeneA_Called[['gene','chr','BreakPoint']].rename(
																columns={'gene':'GeneA','chr':'GeneA_chr','BreakPoint':'GeneA_BreakPoint'})
							df_GeneB_Called = df_GeneB_Called[['gene','chr','BreakPoint']].rename(
																columns={'gene':'GeneB','chr':'GeneB_chr','BreakPoint':'GeneB_BreakPoint'})
							df_GeneAB_Called = pd.concat([df_GeneA_Called,df_GeneB_Called],axis=1)
							I_GeneAB_BreakPoint = df_GeneAB_Called['GeneA_BreakPoint'][0]
							I_Depth = len(set([ PileupRead.alignment.qname 
													for read in Ps_inBAM.pileup(S_GeneA_Chr,I_GeneAB_BreakPoint,I_GeneAB_BreakPoint+1)
													for PileupRead in read.pileups ]))
							F_VAF = round(I_Alt/I_Depth,3)
							#----------------------------------------------------------------------------
							df_GeneAB_Called.insert(loc=df_GeneAB_Called.shape[1],column='Depth',value=[I_Depth])
							df_GeneAB_Called.insert(loc=df_GeneAB_Called.shape[1],column='Alt',value=[I_Alt])
							df_GeneAB_Called.insert(loc=df_GeneAB_Called.shape[1],column='VAF',value=[F_VAF])
							L_ReportDFs.append(df_GeneAB_Called)
	#---------------------------------------------------------------------------------------------------- report
	try :
		df_RawReport = pd.concat(L_RawDFs,ignore_index=True).drop_duplicates()
		df_Report = pd.concat(L_ReportDFs,ignore_index=True)
		df_RawReport.to_csv('{}/{}_Summary.tsv'.format(outdir,S_inName),sep='\t',index=None)
		df_Report.to_csv('{}/{}_FusionReport.tsv'.format(outdir,S_inName),sep='\t',index=None)
	except :
		pass
######################################################################################################### run
if __name__ == '__main__' :
	M_parser = argparse.ArgumentParser(description=textwrap.dedent('''\n
		FUSION Caller.
		\n'''),add_help=False,formatter_class=argparse.RawTextHelpFormatter)
	#-------------------------------------------------------------------------
	M_parser_Required = M_parser.add_argument_group('Required arguments')
	M_parser_Required.add_argument('-ibam','--input-bam',type=str,required=True,
		help=textwrap.dedent('''\
			* Required: Please provide the PATH of input BAM file.
			\n'''))
	M_parser_Required.add_argument('-odir','--output-directory',type=str,required=True,
		help='* Required: Please provide the PATH of output directory.')
	#-------------------------------------------------------------------------
	M_parser_Optional = M_parser.add_argument_group('Optional arguments')
	M_parser_Optional.add_argument('-ibed','--input-bed',type=str,
		default='/ess/prodev/users/dknam/laboratory/Fusion/data/BEDs/FUSION_WholeTarget.bed',
		help=textwrap.dedent('''\
			Please provide the PATH of input BED file.
			This BED file must have specific target gene NAME.
			default) /ess/prodev/users/dknam/laboratory/Fusion/data/BEDs/FUSION_WholeTarget.bed
			\n'''))
	M_parser_Optional.add_argument('-mq','--mapping-quality',type=int,
		default=20,
		help=textwrap.dedent('''\
			Please provide the MINIMUM of mapping quality.
			default) 20
			\n'''))
	M_parser_Optional.add_argument('-nm','--number-mismatch',type=int,
		default=10,
		help=textwrap.dedent('''\
			Please provide the MAXINUM of number of mismatch.
			default) 10
			\n'''))
	M_parser_Optional.add_argument('-cl','--clipped-length',type=int,
		default=10,
		help=textwrap.dedent('''\
			Please provide the MINIMUM of clipped read length.
			default) 10
			\n'''))
	M_parser_Optional.add_argument('-cr','--clipping-ratio',type=float,
		default=15,
		help=textwrap.dedent('''\
			Please provide the MINIMUM of clipping ratio.
			default) 15
			\n'''))
	M_parser_Optional.add_argument('-al','--allowed-length',type=int,
		default=5,
		help=textwrap.dedent('''\
			Please provide the MAXIMUM of allowed length.
			default) 5
			\n'''))
	M_parser_Optional.add_argument('-sd','--supporting-depth',type=int,
		default=10,
		help=textwrap.dedent('''\
			Please provide the MINIMUM of supporting depth.
			default) 10
			\n'''))
	M_parser_Optional.add_argument('-mp','--multi-process',type=int,
		default=5,
		help=textwrap.dedent('''\
			Please provide the MINIMUM of supporting depth.
			default) 5
			\n'''))
	M_parser_Optional.add_argument('-h','--help',action='help', help='show this help message and exit')
	#=========================================================================
	M_args = M_parser.parse_args()
	#-------------------------------------------------------------------------
	S_inBAM = M_args.input_bam
	S_outDir = M_args.output_directory
	#-------------------------------------------------------------------------
	S_inBED = M_args.input_bed
	I_mq = M_args.mapping_quality
	I_nm = M_args.number_mismatch
	I_cl = M_args.clipped_length
	F_cr = M_args.clipping_ratio
	I_al = M_args.allowed_length
	I_sd = M_args.supporting_depth
	I_mp =M_args.multi_process
	#=========================================================================
	S_inName = S_inBAM.split('/')[-1].split('.')[0]
	S_outDir1 = '{}/{}_FUSION'.format(S_outDir,S_inName)
	os.system('mkdir {}'.format(S_outDir1))
	#=========================================================================
	FUSION_caller(S_inBED,S_inBAM,S_outDir1,
					I_mq,I_nm,I_cl,F_cr,
					I_al,I_sd)
	#=========================================================================
#########################################################################################################