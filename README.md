# Targeted-seq FUSION caller
### Detect FUSION in a plasma sample using targeted sequence data.

## GETTING HELP
If your problem or question is still unsolved, feel free to post a question or submit a bug report on the e-mail(dk.nam@kr-geninus.com).

## QUICKSTART GUIDE
Obtain the required modules:
`pip install numpy pandas pysam`  
You may need to install pip first, and depending on preferences you may add sudo at the beginning of this command.

## REQUIREMENTS
This caller was developed and tested using Python3.7. Using any other version may cause errors or faulty results.

The list of python packages required and tested versions as reported by `pip freeze`:  
pandas==0.24.2  
pysam==0.18.0

### script run
To get FUSION plots and result files, use FUSION_Caller.py:  
`python FUSION_Caller.py -ibam [input bam file] -odir [output directory] -ibed [input bed file]`

The target ROI.bed file specified here is GENINUS target ROI. You can take any .bed file here (assuming it fits in memory).    
There are some parameters to adjust trimming, quality filter and variants call.

### In general
To improve your results you probably want to change a few parameters. Most of the values used in caller can be altered using arguments. To find out what arguments can be passed into any script, try running it with -h as argument, for example:  
`python FUSION_Caller.py -h`  

### Description
	usage: FUSION_Caller.py -ibam INPUT_BAM -odir OUTPUT_DIRECTORY
							[-ibed INPUT_BED] [-mq MAPPING_QUALITY]
							[-nm NUMBER_MISMATCH] [-cl CLIPPED_LENGTH]
							[-cr CLIPPING_RATIO] [-al ALLOWED_LENGTH]
							[-sd SUPPORTING_DEPTH] [-mp MULTI_PROCESS] [-h]

	FUSION Caller.

	Required arguments:
	  -ibam INPUT_BAM, --input-bam INPUT_BAM
							* Required: Please provide the PATH of input BAM file.

	  -odir OUTPUT_DIRECTORY, --output-directory OUTPUT_DIRECTORY
							* Required: Please provide the PATH of output directory.

	Optional arguments:
	  -ibed INPUT_BED, --input-bed INPUT_BED
							Please provide the PATH of input BED file.
							This BED file must have specific target gene NAME.
							default) /ess/prodev/users/dknam/laboratory/Fusion/data/BEDs/FUSION_WholeTarget.bed

	  -mq MAPPING_QUALITY, --mapping-quality MAPPING_QUALITY
							Please provide the MINIMUM of mapping quality.
							default) 20

	  -nm NUMBER_MISMATCH, --number-mismatch NUMBER_MISMATCH
							Please provide the MAXINUM of number of mismatch.
							default) 10

	  -cl CLIPPED_LENGTH, --clipped-length CLIPPED_LENGTH
							Please provide the MINIMUM of clipped read length.
							default) 10

	  -cr CLIPPING_RATIO, --clipping-ratio CLIPPING_RATIO
							Please provide the MINIMUM of clipping ratio.
							default) 15

	  -al ALLOWED_LENGTH, --allowed-length ALLOWED_LENGTH
							Please provide the MAXIMUM of allowed length.
							default) 5

	  -sd SUPPORTING_DEPTH, --supporting-depth SUPPORTING_DEPTH
							Please provide the MINIMUM of supporting depth.
							default) 10

	  -mp MULTI_PROCESS, --multi-process MULTI_PROCESS
							Please provide the MINIMUM of supporting depth.
							default) 5

	  -h, --help            show this help message and exit